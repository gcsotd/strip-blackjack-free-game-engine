Strip Blackjack Free Game Engine

The idea:
A simple Strip Blackjack game Engine developed with GODOT; totally FOSS and target primary for HTML5 desktop and mobile browsers, but also for desktop pc's and mobile apps.
The idea is to have an engine ready to be easily fueled with media content (videoclips, but also images and text) and then easily deployed. Of course the avaibility of source code will made this engine also fully personalizable for all needs.

Why this idea:
At todays a really FOSS strip-game engine doesn't exist. It's strange because this kind of games exists since early 80s and of course the engine is very simple to realize... I want to try to resolve this lack so lot of artists (viedomakers, photographers) can be free to concentrate to the media content, the artistic side of the job, wich is the most important thing in games of this kind. I have choosed to start from a blackjack game because imho has the best gameplay for this purpose and i choosed Godot engine because it can export to virtually any platform.

The gameplay:
Standard blackjack rules, with some little personalization, where the human player will play against the AI model wich of course will also be the subject of the media content.
The game will be divided into levels, where each level corrisponds to a peace of clothing that the model will remove. 
During all the game lenght, media contents (videoclips and/or still images and/or text) will be reproduced randomly based on wich level is playing.
When the human player wins 3 hands in a row the model will remove a piece of clothing and the game will go into the next level. When the human player loses 3 hand in a row he will virtually lose a piece of clothes, if he will lose 5 clothes he will lose the game.
The goal is, like all strip games, to totally undress the opponent.