extends Sprite

const R_ACE = 1
const R_TWO = 2
const R_THREE = 3
const R_FOUR = 4
const R_FIVE = 5
const R_SIX = 6
const R_SEVEN = 7
const R_EIGHT = 8
const R_NINE = 9
const R_TEN = 10
const R_JACK = 11
const R_QUEEN = 12
const R_KING = 13

#const S_CLUBS = 2
#const S_DIAMONDS = 3
#const S_HEARTS = 1
#const S_SPADES = 0

const S_CLUBS = 0
const S_DIAMONDS = 1
const S_HEARTS = 2
const S_SPADES = 3

export(int, 1, 13) var rank = R_TWO
export(int, 0, 3) var suit = S_CLUBS
export(bool) var face_up = true

func is_face_up():
	return face_up

func set_face_up(face_up):
	self.face_up = face_up
	apply()

func set_rank(rank):
	self.rank = rank
	apply()


func set_suit(suit):
	self.suit = suit
	apply()

func init_card(rank, suit, face_up):
	self.rank = rank
	self.suit = suit
	self.face_up = face_up
	apply()


func flip():
	face_up = !face_up
	apply()


func apply():
	if is_face_up():
		var idx = suit*13 + rank - 1
		set_frame(idx)
	else:
		set_frame(52)


func get_value():
	#if a card is face down, its value is 0
	var value = 0
	
	if is_face_up():
		#value is number showing on card
		value = rank
		#value is 10 for face cards
		if value > 10:
			value = 10;

	return value;


func _ready():
	apply()
	pass
