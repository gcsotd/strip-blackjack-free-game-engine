extends "generic_player.gd"


func shoot():
	return get_total() <= 16


func flip_first_card():
	if !cards.empty():
		cards[0].flip()
	else:
		print("No card to flip!")


