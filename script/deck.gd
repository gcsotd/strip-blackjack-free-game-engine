extends "hand.gd"

var card_inst = preload("res://prefab/card.scn")

var save_carsd = []

func populate():

	clear()

	if save_carsd.empty():
		#create deck
		for s in range(0, 4):
			for r in range(1, 14):
				var c = card_inst.instance()
				c.init_card(r, s, false)
				add_card(c)
				
		save_carsd = Array(cards)

	else:
		cards = Array(save_carsd)
		
		for c in cards:
			c.set_face_up(false)
			add_card(c)
			
		applay()
		

func shuffle():
	var temp = []

	while !cards.empty():
		var i = randi()%cards.size()
		temp.push_back(cards[i])
		cards.remove(i)

	cards = temp


func deal(aHand):

	if !cards.empty():
		var card = cards[cards.size()-1]
		
		cards.remove(cards.size()-1)
		remove_child(card)

		aHand.add_card(card)
		return card
	else:
		print("Out of cards, Unable to deal.")
		#cout << "Out of cards, Unable to deal.\n";

	return null


func additional_cards(aGenericPlayer, face_up):

	#cout << endl;
	#continue to deal a card as long as generic player isn't busted and
	#wants another hit
	while (!(aGenericPlayer.is_busted()) && aGenericPlayer.shoot()):
		var card = deal(aGenericPlayer);

		if card && face_up:
			card.flip()

		if aGenericPlayer.is_busted():
			aGenericPlayer.bust()


func _ready():
	#card_offset = Vector(4, 0)
	pass

